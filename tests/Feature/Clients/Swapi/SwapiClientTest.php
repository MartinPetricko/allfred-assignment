<?php

namespace Tests\Feature\Clients\Swapi;

use Tests\TestCase;
use App\Clients\Swapi\SwapiClient;

class SwapiClientTest extends TestCase
{
    private SwapiClient $client;

    public function setUp(): void
    {
        parent::setUp();

        $this->client = app(SwapiClient::class);
    }

    public function test_can_fetch_planets(): void
    {
        $this->client->getPlanets();

        $this->assertTrue(true);
    }

    public function test_can_fetch_people(): void
    {
        $this->client->getPeople();

        $this->assertTrue(true);
    }

    public function test_can_fetch_species(): void
    {
        $this->client->getSpecies();

        $this->assertTrue(true);
    }
}
