<x-layouts.app title="Planets">
    <div class="container mx-auto">

        <div>
            <form action="{{ route('planets.index') }}">
                <input type="hidden" name="page" value="{{ request()->query('page') }}">
                <input type="number" name="min_diameter" placeholder="Minimum diameter" value="{{ request()->query('min_diameter') }}">
                <input type="number" name="max_diameter" placeholder="Maximum diameter" value="{{ request()->query('max_diameter') }}">
                <input type="number" name="min_rotation_period" placeholder="Minimum rotation period" value="{{ request()->query('min_rotation_period') }}">
                <input type="number" name="max_rotation_period" placeholder="Maximum rotation period" value="{{ request()->query('max_rotation_period') }}">
                <input type="number" name="min_gravity" placeholder="Minimum gravity" step="0.01" value="{{ request()->query('min_gravity') }}">
                <input type="number" name="max_gravity" placeholder="Maximum gravity" step="0.01" value="{{ request()->query('max_gravity') }}">

                <button type="submit">Filter</button>
            </form>
        </div>

        <table>
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Diameter</th>
                    <th>Rotation period</th>
                    <th>Orbital period</th>
                    <th>Gravity</th>
                    <th>Population</th>
                    <th>Climate</th>
                    <th>Terrain</th>
                    <th>Surface water</th>
                </tr>
            </thead>
            <tbody>
                @foreach($planets as $planet)
                    <tr>
                        <th>{{ $planet->id }}</th>
                        <td>{{ $planet->name }}</td>
                        <td>{{ $planet->diameter }}</td>
                        <td>{{ $planet->rotation_period }}</td>
                        <td>{{ $planet->orbital_period }}</td>
                        <td>{{ $planet->gravity }}</td>
                        <td>{{ $planet->population }}</td>
                        <td>{{ $planet->climate }}</td>
                        <td>{{ $planet->terrain }}</td>
                        <td>{{ $planet->surface_water }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div>
            {!! $planets->links() !!}
        </div>
    </div>
</x-layouts.app>
