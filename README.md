## API documentation

[Documentation](docs/api.md)

## Docker Initialization

### 1. Bash alias

```bash
alias dcomposer='docker-compose run --rm composer'
alias dnpm='docker-compose run --rm npm'
alias dphp='docker-compose run --rm php'
```

```bash
source ~/.bashrc
```

### 2. Docker compose

```bash
docker-compose up
```

Application is available at

```
http://localhost:8000
```

For project initialization with docker use

```bash
dcomposer
dphp
dnpm
```

## Project Initialization

### 1. Copy .env.example to .env

```bash
cp .env.example .env
```

### 2. Install dependencies

```bash
composer install
```

### 3. Generate application key and migrate database

```bash
php artisan key:generate
php artisan migrate --seed
```

### 4. Link storage to public folder

```bash
php artisan storage:link --relative
```

### 5. Change storage permissions

```bash
chmod -R 777 storage/
```

### 6. Add CRON job on server

```bash
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```
