<?php

namespace App\Actions\Api\Log;

use App\Models\Log;
use App\Services\Log\LogServiceInterface;
use App\Http\Requests\Api\Log\StoreLogRequest;

class StoreLogAction
{
    private LogServiceInterface $logService;

    public function __construct(LogServiceInterface $logService)
    {
        $this->logService = $logService;
    }

    public function handle(StoreLogRequest $request): Log
    {
        $log = $this->logService->create();

        $log->fill($request->validated());

        return $this->logService->save($log);
    }
}
