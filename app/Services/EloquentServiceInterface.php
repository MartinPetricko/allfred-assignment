<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

interface EloquentServiceInterface
{
    public function create(): Model;

    public function save(Model $model): Model;

    public function delete(Model $model): void;

    public function restore(Model $model): void;

    public function forceDelete(Model $model): void;

    public function upsert(array $values, array|string $uniqueBy, array $update = null): int;
}
