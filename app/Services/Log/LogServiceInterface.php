<?php

namespace App\Services\Log;

use App\Models\Log;
use App\Services\EloquentServiceInterface;
use Illuminate\Database\Eloquent\Model;

interface LogServiceInterface extends EloquentServiceInterface
{
    public function create(): Model|Log;

    public function save(Model|Log $model): Model|Log;

    public function delete(Model|Log $model): void;

    public function restore(Model|Log $model): void;

    public function forceDelete(Model|Log $model): void;
}
