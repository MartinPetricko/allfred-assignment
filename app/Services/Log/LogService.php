<?php

namespace App\Services\Log;

use App\Repositories\Log\LogRepositoryInterface;
use App\Services\EloquentService;

class LogService extends EloquentService implements LogServiceInterface
{
    public function __construct(LogRepositoryInterface $repository)
    {
        parent::__construct($repository);
    }
}
