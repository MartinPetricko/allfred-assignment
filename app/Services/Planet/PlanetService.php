<?php

namespace App\Services\Planet;

use App\Repositories\Planet\PlanetRepositoryInterface;
use App\Services\EloquentService;

class PlanetService extends EloquentService implements PlanetServiceInterface
{
    public function __construct(PlanetRepositoryInterface $repository)
    {
        parent::__construct($repository);
    }
}
