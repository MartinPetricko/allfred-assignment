<?php

namespace App\Services\Planet;

use App\Models\Planet;
use App\Services\EloquentServiceInterface;
use Illuminate\Database\Eloquent\Model;

interface PlanetServiceInterface extends EloquentServiceInterface
{
    public function create(): Model|Planet;

    public function save(Model|Planet $model): Model|Planet;

    public function delete(Model|Planet $model): void;

    public function restore(Model|Planet $model): void;

    public function forceDelete(Model|Planet $model): void;
}
