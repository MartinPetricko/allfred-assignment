<?php

namespace App\Services\Species;

use App\Repositories\Species\SpeciesRepositoryInterface;
use App\Services\EloquentService;

class SpeciesService extends EloquentService implements SpeciesServiceInterface
{
    public function __construct(SpeciesRepositoryInterface $repository)
    {
        parent::__construct($repository);
    }
}
