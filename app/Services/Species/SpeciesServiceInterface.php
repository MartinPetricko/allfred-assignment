<?php

namespace App\Services\Species;

use App\Models\Species;
use App\Services\EloquentServiceInterface;
use Illuminate\Database\Eloquent\Model;

interface SpeciesServiceInterface extends EloquentServiceInterface
{
    public function create(): Model|Species;

    public function save(Model|Species $model): Model|Species;

    public function delete(Model|Species $model): void;

    public function restore(Model|Species $model): void;

    public function forceDelete(Model|Species $model): void;
}
