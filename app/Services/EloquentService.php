<?php

namespace App\Services;

use App\Repositories\EloquentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

abstract class EloquentService implements EloquentServiceInterface
{
    protected EloquentRepositoryInterface $repository;

    public function __construct(EloquentRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function create(): Model
    {
        return clone $this->repository->getModel();
    }

    public function save(Model $model): Model
    {
        $this->repository->save($model);

        return $model;
    }

    public function delete(Model $model): void
    {
        $this->repository->delete($model);
    }

    public function restore(Model $model): void
    {
        $this->repository->restore($model);
    }

    public function forceDelete(Model $model): void
    {
        $this->repository->forceDelete($model);
    }

    public function upsert(array $values, array|string $uniqueBy, array $update = null): int
    {
        return $this->repository->upsert($values, $uniqueBy, $update);
    }
}
