<?php

namespace App\Services\Person;

use App\Models\Person;
use App\Services\EloquentServiceInterface;
use Illuminate\Database\Eloquent\Model;

interface PersonServiceInterface extends EloquentServiceInterface
{
    public function create(): Model|Person;

    public function save(Model|Person $model): Model|Person;

    public function delete(Model|Person $model): void;

    public function restore(Model|Person $model): void;

    public function forceDelete(Model|Person $model): void;
}
