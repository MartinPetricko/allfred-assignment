<?php

namespace App\Services\Person;

use App\Repositories\Person\PersonRepositoryInterface;
use App\Services\EloquentService;

class PersonService extends EloquentService implements PersonServiceInterface
{
    public function __construct(PersonRepositoryInterface $repository)
    {
        parent::__construct($repository);
    }
}
