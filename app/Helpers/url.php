<?php

if (!function_exists('encodeUrl')) {
    function encodeUrl(string $url): string
    {
        return asset(implode('/', array_map(static fn ($v) => rawurlencode($v), explode('/', parse_url($url)['path']))));
    }
}
