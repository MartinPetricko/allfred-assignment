<?php

if (!function_exists('stub')) {
    function stub(string $stub, array $variables = []): string
    {
        $contents = file_get_contents(base_path("stubs/$stub.stub"));

        foreach ($variables as $key => $value) {
            $contents = str_replace('{{ ' . $key . ' }}', $value, $contents);
        }

        return $contents;
    }
}
