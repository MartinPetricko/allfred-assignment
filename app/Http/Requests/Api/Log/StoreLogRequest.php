<?php

namespace App\Http\Requests\Api\Log;

use Illuminate\Foundation\Http\FormRequest;

class StoreLogRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'weather'             => ['required', 'string', 'max:255'],
            'temperature'         => ['required', 'string', 'max:255'],
            'mood'                => ['required', 'string', 'max:255'],
            'location'            => ['nullable', 'string', 'max:255'],
            'accomplishments'     => ['nullable', 'string', 'max:1024'],
            'observations'        => ['nullable', 'string', 'max:2048'],
            'health_condition'    => ['required', 'string'],
            'ship_condition'      => ['nullable', 'string'],
            'radio_communication' => ['nullable', 'string'],
            'inventory'           => ['required', 'string'],
            'note'                => ['required', 'string'],
        ];
    }
}
