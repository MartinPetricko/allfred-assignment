<?php

namespace App\Http\Resources;

use App\Models\Planet;
use Illuminate\Http\Resources\Json\JsonResource;

class PlanetResource extends JsonResource
{
    public function toArray($request): array
    {
        /**
         * @var Planet|PlanetResource $this
         */
        return [
            'id'              => $this->id,
            'name'            => $this->name,
            'diameter'        => $this->diameter,
            'rotation_period' => $this->rotation_period,
            'orbital_period'  => $this->orbital_period,
            'gravity'         => $this->gravity,
            'population'      => $this->population,
            'climate'         => $this->climate,
            'terrain'         => $this->terrain,
            'surface_water'   => $this->surface_water,
            'created_at'      => $this->created_at,
            'updated_at'      => $this->updated_at,
        ];
    }
}
