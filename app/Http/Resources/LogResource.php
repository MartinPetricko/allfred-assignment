<?php

namespace App\Http\Resources;

use App\Models\Log;
use Illuminate\Http\Resources\Json\JsonResource;

class LogResource extends JsonResource
{
    public function toArray($request): array
    {
        /**
         * @var Log|LogResource $this
         */
        return [
            'id'                  => $this->id,
            'weather'             => $this->weather,
            'temperature'         => $this->temperature,
            'mood'                => $this->mood,
            'location'            => $this->location,
            'accomplishments'     => $this->accomplishments,
            'observations'        => $this->observations,
            'health_condition'    => $this->health_condition,
            'ship_condition'      => $this->ship_condition,
            'radio_communication' => $this->radio_communication,
            'inventory'           => $this->inventory,
            'note'                => $this->note,
            'created_at'          => $this->created_at,
            'updated_at'          => $this->updated_at,
        ];
    }
}
