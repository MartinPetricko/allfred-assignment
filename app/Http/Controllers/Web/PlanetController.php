<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use App\Http\Controllers\Controller;
use App\Repositories\Planet\PlanetRepositoryInterface;

class PlanetController extends Controller
{
    private PlanetRepositoryInterface $planetRepository;

    public function __construct(PlanetRepositoryInterface $planetRepository)
    {
        $this->planetRepository = $planetRepository;
    }

    public function index(Request $request): View
    {
        return view('planets.index', [
            'planets' => $this->planetRepository->get(filter: $request->query()),
        ]);
    }
}
