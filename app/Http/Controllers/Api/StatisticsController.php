<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\PlanetResource;
use App\Repositories\Planet\PlanetRepositoryInterface;
use App\Repositories\Species\SpeciesRepositoryInterface;

class StatisticsController extends Controller
{
    private PlanetRepositoryInterface $planetRepository;

    private SpeciesRepositoryInterface $speciesRepository;

    public function __construct(PlanetRepositoryInterface $planetRepository, SpeciesRepositoryInterface $speciesRepository)
    {
        $this->planetRepository  = $planetRepository;
        $this->speciesRepository = $speciesRepository;
    }

    public function index(): JsonResponse
    {
        return new JsonResponse([
            'largest_planets'       => PlanetResource::collection($this->planetRepository->getLargest(10)),
            'terrain_distributions' => $this->planetRepository->getTerrainDistributionPercentage(),
            'species_distributions' => $this->speciesRepository->getSpeciesDistributionPercentage(),
        ]);
    }
}
