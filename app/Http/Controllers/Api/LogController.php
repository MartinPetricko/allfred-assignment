<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\LogResource;
use App\Http\Controllers\Controller;
use App\Actions\Api\Log\StoreLogAction;
use App\Http\Requests\Api\Log\StoreLogRequest;
use App\Repositories\Log\LogRepositoryInterface;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class LogController extends Controller
{
    private LogRepositoryInterface $logRepository;

    public function __construct(LogRepositoryInterface $logRepository)
    {
        $this->logRepository = $logRepository;
    }

    public function index(): AnonymousResourceCollection
    {
        return LogResource::collection($this->logRepository->all());
    }

    public function store(StoreLogRequest $request, StoreLogAction $action): LogResource
    {
        return new LogResource($action->handle($request));
    }
}
