<?php

namespace App\Console\Commands\Swapi;

use Illuminate\Console\Command;
use App\Clients\Swapi\SwapiClient;
use Illuminate\Support\Facades\DB;
use App\Services\Planet\PlanetServiceInterface;
use App\Services\Person\PersonServiceInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Services\Species\SpeciesServiceInterface;

class SyncData extends Command
{
    protected $signature = 'swapi:sync';

    protected $description = 'Synchronize data from swapi';

    private SwapiClient $swapiClient;

    private PlanetServiceInterface $planetService;

    private PersonServiceInterface $personService;

    private SpeciesServiceInterface $speciesService;

    public function __construct()
    {
        parent::__construct();

        $this->swapiClient    = app(SwapiClient::class);
        $this->planetService  = app(PlanetServiceInterface::class);
        $this->personService  = app(PersonServiceInterface::class);
        $this->speciesService = app(SpeciesServiceInterface::class);
    }

    public function handle(): int
    {
        $this->syncPlanets();

        $this->syncSpecies();

        $this->syncPeople();

        return 0;
    }

    private function syncPlanets(): void
    {
        $this->info('Synchronizing planets.');

        $progressBar = $this->output->createProgressBar();

        $progressBar->setFormat(ProgressBar::FORMAT_VERY_VERBOSE);

        $progressBar->start();

        $page = 1;

        do {
            $response = $this->swapiClient->getPlanets($page);

            if (!$progressBar->getMaxSteps()) {
                $progressBar->setMaxSteps($response->getCount());
            }

            $planetsToInsert = [];

            foreach ($response->getPlanets() as $planet) {
                $planet = $planet->toArray();

                $planet['diameter']        = $planet['diameter'] === 'unknown' ? null : (int)$planet['diameter'];
                $planet['rotation_period'] = $planet['rotation_period'] === 'unknown' ? null : (int)$planet['rotation_period'];
                $planet['orbital_period']  = $planet['orbital_period'] === 'unknown' ? null : (int)$planet['orbital_period'];
                $planet['population']      = $planet['population'] === 'unknown' ? null : (int)$planet['population'];
                $planet['surface_water']   = $planet['surface_water'] === 'unknown' ? null : (float)$planet['surface_water'];

                unset($planet['residents'], $planet['url']);

                $planetsToInsert[] = $planet;

                $progressBar->advance();
            }

            $this->planetService->upsert($planetsToInsert, 'id');

            $page = $response->getNextPage();
        } while ($page);

        $progressBar->finish();

        $this->newLine(2);
    }

    private function syncPeople(): void
    {
        $this->info('Synchronizing people.');

        $progressBar = $this->output->createProgressBar();

        $progressBar->setFormat(ProgressBar::FORMAT_VERY_VERBOSE);

        $progressBar->start();

        $page = 1;

        do {
            $response = $this->swapiClient->getPeople($page);

            if (!$progressBar->getMaxSteps()) {
                $progressBar->setMaxSteps($response->getCount());
            }

            $peopleToInsert = [];
            $person_species = [];

            foreach ($response->getPeople() as $person) {
                foreach ($person->getSpecies() as $species) {
                    $person_species[] = [
                        'person_id'  => $person->getId(),
                        'species_id' => $species,
                    ];
                }

                $person = $person->toArray();

                $person['planet_id'] = $person['homeworld'];

                unset($person['homeworld'], $person['species'], $person['url']);

                $peopleToInsert[] = $person;

                $progressBar->advance();
            }

            $this->personService->upsert($peopleToInsert, 'id');

            DB::table('person_species')->upsert($person_species, ['person_id', 'species_id']);

            $page = $response->getNextPage();
        } while ($page);

        $progressBar->finish();

        $this->newLine(2);
    }

    private function syncSpecies(): void
    {
        $this->info('Synchronizing species.');

        $progressBar = $this->output->createProgressBar();

        $progressBar->setFormat(ProgressBar::FORMAT_VERY_VERBOSE);

        $progressBar->start();

        $page = 1;

        do {
            $response = $this->swapiClient->getSpecies($page);

            if (!$progressBar->getMaxSteps()) {
                $progressBar->setMaxSteps($response->getCount());
            }

            $speciesToInsert = [];

            foreach ($response->getSpecies() as $species) {
                $species = $species->toArray();

                $species['planet_id'] = $species['homeworld'];

                unset($species['homeworld'], $species['people'], $species['url']);

                $speciesToInsert[] = $species;

                $progressBar->advance();
            }

            $this->speciesService->upsert($speciesToInsert, 'id');

            $page = $response->getNextPage();
        } while ($page);

        $progressBar->finish();

        $this->newLine(2);
    }
}
