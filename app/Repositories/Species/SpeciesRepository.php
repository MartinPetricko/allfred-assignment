<?php

namespace App\Repositories\Species;

use App\Models\Species;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\EloquentRepository;

class SpeciesRepository extends EloquentRepository implements SpeciesRepositoryInterface
{
    protected Model|Species $model;

    public function __construct(Species $model)
    {
        parent::__construct($model);
    }

    public function getSpeciesDistribution(): Collection
    {
        $person_species = collect();

        foreach (DB::table('person_species')->get()->groupBy('species_id') as $species_id => $relations) {
            $person_species[$species_id] = $relations->pluck('person_id');
        }

        $people = DB::table('people')->select(['id', 'planet_id'])->pluck('planet_id', 'id');

        foreach ($person_species as $species_id => $species) {
            foreach ($species as $person_id => $person) {
                $person_species[$species_id][$person_id] = $people[$person];
            }

            $person_species[$species_id] = $person_species[$species_id]->count();
        }

        $species = DB::table('species')->select(['id', 'name'])->pluck('name', 'id');

        $result = collect();

        foreach ($person_species as $species_id => $count) {
            $result[$species[$species_id]] = $count;
        }

        return $result;
    }

    public function getSpeciesDistributionPercentage(): Collection
    {
        $species = $this->getSpeciesDistribution();

        $number_of_relations = $species->sum();

        foreach ($species as $key => $terrain) {
            $species[$key] = $terrain / $number_of_relations * 100;
        }

        return $species;
    }
}
