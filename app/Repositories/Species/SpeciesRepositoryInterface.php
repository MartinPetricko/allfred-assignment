<?php

namespace App\Repositories\Species;

use Closure;
use App\Models\Species;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Expression;
use App\Repositories\EloquentRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface SpeciesRepositoryInterface extends EloquentRepositoryInterface
{
    public function getModel(): Model|Species;

    public function all(): Collection|Model|Species;

    public function where(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Collection|Model|Species;

    public function get(array $filter = null): LengthAwarePaginator|Collection|Model|Species;

    public function find(mixed $id): Model|Species|null|Collection;

    public function findOrFail(mixed $id): Model|Species;

    public function first(): Model|Species|null;

    public function firstOrFail(): Model|Species;

    public function firstWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Model|Species|null;

    public function firstOrFailWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Model|Species;

    public function save(Model|Species $model): Model|Species;

    public function delete(Model|Species $model): void;

    public function restore(Model|Species $model): void;

    public function forceDelete(Model|Species $model): void;

    public function getSpeciesDistribution(): Collection;

    public function getSpeciesDistributionPercentage(): Collection;
}
