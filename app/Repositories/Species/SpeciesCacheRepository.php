<?php

namespace App\Repositories\Species;

use App\Repositories\HasBasicCacheMethods;

class SpeciesCacheRepository extends SpeciesRepository
{
    use HasBasicCacheMethods;
}
