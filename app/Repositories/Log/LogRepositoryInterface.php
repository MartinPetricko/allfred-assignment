<?php

namespace App\Repositories\Log;

use Closure;
use App\Models\Log;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Expression;
use App\Repositories\EloquentRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface LogRepositoryInterface extends EloquentRepositoryInterface
{
    public function getModel(): Model|Log;

    public function all(): Collection|Model|Log;

    public function where(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Collection|Model|Log;

    public function get(array $filter = null): LengthAwarePaginator|Collection|Model|Log;

    public function find(mixed $id): Model|Log|null|Collection;

    public function findOrFail(mixed $id): Model|Log;

    public function first(): Model|Log|null;

    public function firstOrFail(): Model|Log;

    public function firstWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Model|Log|null;

    public function firstOrFailWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Model|Log;

    public function save(Model|Log $model): Model|Log;

    public function delete(Model|Log $model): void;

    public function restore(Model|Log $model): void;

    public function forceDelete(Model|Log $model): void;
}
