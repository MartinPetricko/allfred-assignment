<?php

namespace App\Repositories\Log;

use App\Models\Log;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\EloquentRepository;

class LogRepository extends EloquentRepository implements LogRepositoryInterface
{
    protected Model|Log $model;

    public function __construct(Log $model)
    {
        parent::__construct($model);
    }
}
