<?php

namespace App\Repositories\Log;

use App\Repositories\HasBasicCacheMethods;

class LogCacheRepository extends LogRepository
{
    use HasBasicCacheMethods;
}
