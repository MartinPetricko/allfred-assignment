<?php

namespace App\Repositories\Person;

use Closure;
use App\Models\Person;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Expression;
use App\Repositories\EloquentRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface PersonRepositoryInterface extends EloquentRepositoryInterface
{
    public function getModel(): Model|Person;

    public function all(): Collection|Model|Person;

    public function where(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Collection|Model|Person;

    public function get(array $filter = null): LengthAwarePaginator|Collection|Model|Person;

    public function find(mixed $id): Model|Person|null|Collection;

    public function findOrFail(mixed $id): Model|Person;

    public function first(): Model|Person|null;

    public function firstOrFail(): Model|Person;

    public function firstWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Model|Person|null;

    public function firstOrFailWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Model|Person;

    public function save(Model|Person $model): Model|Person;

    public function delete(Model|Person $model): void;

    public function restore(Model|Person $model): void;

    public function forceDelete(Model|Person $model): void;
}
