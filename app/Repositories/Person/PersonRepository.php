<?php

namespace App\Repositories\Person;

use App\Models\Person;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\EloquentRepository;

class PersonRepository extends EloquentRepository implements PersonRepositoryInterface
{
    protected Model|Person $model;

    public function __construct(Person $model)
    {
        parent::__construct($model);
    }
}
