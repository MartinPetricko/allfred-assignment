<?php

namespace App\Repositories\Person;

use App\Repositories\HasBasicCacheMethods;

class PersonCacheRepository extends PersonRepository
{
    use HasBasicCacheMethods;
}
