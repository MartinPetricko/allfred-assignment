<?php

namespace App\Repositories;

use Closure;
use Illuminate\Support\Str;
use Illuminate\Pagination\Cursor;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

abstract class EloquentRepository implements EloquentRepositoryInterface
{
    protected Model $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getModel(): Model
    {
        return $this->model;
    }

    public function getCacheKey(): string
    {
        return Str::plural(Str::lower(class_basename($this->model)));
    }

    public function all(): Collection
    {
        return $this->model::all();
    }

    public function where(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Collection|Model
    {
        return $this->model::where($column, $operator, $value, $boolean);
    }

    public function get(array $filter = null): LengthAwarePaginator|Collection
    {
        return $this->filter($this->model::query(), $filter);
    }

    public function find(mixed $id): null|Model|Collection
    {
        return $this->model::find($id);
    }

    public function findOrFail(mixed $id): Model
    {
        return $this->model::findOrFail($id);
    }

    public function first(): ?Model
    {
        return $this->model::first();
    }

    public function firstOrFail(): Model
    {
        return $this->model::firstOrFail();
    }

    public function firstWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): ?Model
    {
        return $this->model::firstWhere($column, $operator, $value, $boolean);
    }

    public function firstOrFailWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Model
    {
        return $this->where($column, $operator, $value, $boolean)->firstOrFail();
    }

    public function save(Model $model): Model
    {
        $model->save();

        return $model;
    }

    public function delete(Model $model): void
    {
        $model->delete();
    }

    public function restore(Model $model): void
    {
        $model->restore();
    }

    public function forceDelete(Model $model): void
    {
        $model->forceDelete();
    }

    public function upsert(array $values, array|string $uniqueBy, array $update = null): int
    {
        return $this->model->upsert($values, $uniqueBy, $update);
    }

    protected function filter(Builder $builder, ?array $filter, string $modelFilter = null): LengthAwarePaginator|Collection
    {
        if ($filter) {
            $builder->filter($filter, $modelFilter);
        }

        return $builder->paginate($filter['count'] ?? 25)->withQueryString();
    }
}
