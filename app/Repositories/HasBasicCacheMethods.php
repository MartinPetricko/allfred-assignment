<?php

namespace App\Repositories;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;

trait HasBasicCacheMethods
{
    public function all(): Collection
    {
        return Cache::remember($this->getCacheKey(), config('cache.ttl'), fn () => parent::all());
    }

    public function find(mixed $id): null|Model|Collection
    {
        return Cache::remember("{$this->getCacheKey()}.$id", config('cache.ttl'), fn () => parent::find($id));
    }

    public function findOrFail(mixed $id): Model
    {
        return Cache::remember("{$this->getCacheKey()}.$id", config('cache.ttl'), fn () => parent::findOrFail($id));
    }

    public function save(Model $model): Model
    {
        $this->clearCache($model);

        return parent::save($model);
    }

    public function delete(Model $model): void
    {
        $this->clearCache($model);

        parent::delete($model);
    }

    public function restore(Model $model): void
    {
        $this->clearCache($model);

        parent::restore($model);
    }

    public function forceDelete(Model $model): void
    {
        $this->clearCache($model);

        parent::forceDelete($model);
    }

    public function upsert(array $values, array|string $uniqueBy, array $update = null): int
    {
        Cache::forget($this->getCacheKey());

        foreach ($values as $value) {
            Cache::forget("{$this->getCacheKey()}." . $value[$this->getModel()->getKeyName()]);
        }

        return parent::upsert($values, $uniqueBy, $update);
    }

    protected function clearCache(Model $model): void
    {
        Cache::forget($this->getCacheKey());
        Cache::forget("{$this->getCacheKey()}.{$model->getKey()}");
    }
}
