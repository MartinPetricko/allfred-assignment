<?php

namespace App\Repositories\Planet;

use App\Models\Planet;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\EloquentRepository;

class PlanetRepository extends EloquentRepository implements PlanetRepositoryInterface
{
    protected Model|Planet $model;

    public function __construct(Planet $model)
    {
        parent::__construct($model);
    }

    public function getLargest(int $count): Collection|Model|Planet
    {
        return $this->model::orderByDesc('diameter')->limit($count)->get();
    }

    public function getTerrainDistribution(): Collection
    {
        $terrains = collect();

        foreach ($this->model::select('terrain')->distinct()->pluck('terrain') as $planet_terrains) {
            foreach (explode(',', $planet_terrains) as $terrain) {
                $terrains->push(trim($terrain));
            }
        }

        $query = DB::table('planets');

        foreach ($terrains->unique() as $terrain) {
            $query->selectRaw("(SELECT COUNT(*) FROM `planets` WHERE `terrain` LIKE '%$terrain%') as '$terrain'");
        }

        return collect($query->first());
    }

    public function getTerrainDistributionPercentage(): Collection
    {
        $terrains = $this->getTerrainDistribution();

        $number_of_relations = $terrains->sum();

        foreach ($terrains as $key => $terrain) {
            $terrains[$key] = $terrain / $number_of_relations * 100;
        }

        return $terrains;
    }
}
