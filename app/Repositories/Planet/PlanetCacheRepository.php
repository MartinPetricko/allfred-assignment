<?php

namespace App\Repositories\Planet;

use App\Repositories\HasBasicCacheMethods;

class PlanetCacheRepository extends PlanetRepository
{
    use HasBasicCacheMethods;
}
