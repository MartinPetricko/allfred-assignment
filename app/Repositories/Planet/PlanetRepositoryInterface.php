<?php

namespace App\Repositories\Planet;

use Closure;
use App\Models\Planet;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Expression;
use App\Repositories\EloquentRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface PlanetRepositoryInterface extends EloquentRepositoryInterface
{
    public function getModel(): Model|Planet;

    public function all(): Collection|Model|Planet;

    public function where(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Collection|Model|Planet;

    public function get(array $filter = null): LengthAwarePaginator|Collection|Model|Planet;

    public function find(mixed $id): Model|Planet|null|Collection;

    public function findOrFail(mixed $id): Model|Planet;

    public function first(): Model|Planet|null;

    public function firstOrFail(): Model|Planet;

    public function firstWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Model|Planet|null;

    public function firstOrFailWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Model|Planet;

    public function save(Model|Planet $model): Model|Planet;

    public function delete(Model|Planet $model): void;

    public function restore(Model|Planet $model): void;

    public function forceDelete(Model|Planet $model): void;

    public function getLargest(int $count): Collection|Model|Planet;

    public function getTerrainDistribution(): Collection;

    public function getTerrainDistributionPercentage(): Collection;
}
