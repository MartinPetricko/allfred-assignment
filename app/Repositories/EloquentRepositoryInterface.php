<?php

namespace App\Repositories;

use Closure;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Expression;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface EloquentRepositoryInterface
{
    public function getModel(): Model;

    public function getCacheKey(): string;

    public function all(): Collection|Model;

    public function where(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Collection|Model;

    public function get(array $filter = null): LengthAwarePaginator|Collection|Model;

    public function find(mixed $id): null|Model|Collection;

    public function findOrFail(mixed $id): Model;

    public function first(): ?Model;

    public function firstOrFail(): Model;

    public function firstWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): ?Model;

    public function firstOrFailWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Model;

    public function save(Model $model): Model;

    public function delete(Model $model): void;

    public function restore(Model $model): void;

    public function forceDelete(Model $model): void;

    public function upsert(array $values, array|string $uniqueBy, array $update = null): int;
}
