<?php

namespace App\Filters;

use EloquentFilter\ModelFilter;

class PlanetFilter extends ModelFilter
{
    public function minDiameter($value): PlanetFilter
    {
        return $this->where('diameter', '>=', $value);
    }

    public function maxDiameter($value): PlanetFilter
    {
        return $this->where('diameter', '<=', $value);
    }

    public function minRotationPeriod($value): PlanetFilter
    {
        return $this->where('rotation_period', '>=', $value);
    }

    public function maxRotationPeriod($value): PlanetFilter
    {
        return $this->where('rotation_period', '<=', $value);
    }

    public function minGravity($value): PlanetFilter
    {
        return $this->where('gravity', '>=', $value);
    }

    public function maxGravity($value): PlanetFilter
    {
        return $this->where('gravity', '<=', $value);
    }
}
