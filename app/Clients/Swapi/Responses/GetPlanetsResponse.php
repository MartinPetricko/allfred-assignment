<?php

namespace App\Clients\Swapi\Responses;

use App\Entities\Swapi\Planet;

class GetPlanetsResponse
{
    private int $count;

    private int $page;

    private ?int $previous_page;

    private ?int $next_page;

    /**
     * @var Planet[]
     */
    private array $planets;

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    public function getPreviousPage(): ?int
    {
        return $this->previous_page;
    }

    public function setPreviousPage(?int $previous_page): void
    {
        $this->previous_page = $previous_page;
    }

    public function getNextPage(): ?int
    {
        return $this->next_page;
    }

    public function setNextPage(?int $next_page): void
    {
        $this->next_page = $next_page;
    }

    public function getPlanets(): array
    {
        return $this->planets;
    }

    public function setPlanets(array $planets): void
    {
        $this->planets = $planets;
    }
}
