<?php

namespace App\Clients\Swapi\Responses;

use App\Entities\Swapi\Person;

class GetPeopleResponse
{
    private int $count;

    private int $page;

    private ?int $previous_page;

    private ?int $next_page;

    /**
     * @var Person[]
     */
    private array $people;

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    public function getPreviousPage(): ?int
    {
        return $this->previous_page;
    }

    public function setPreviousPage(?int $previous_page): void
    {
        $this->previous_page = $previous_page;
    }

    public function getNextPage(): ?int
    {
        return $this->next_page;
    }

    public function setNextPage(?int $next_page): void
    {
        $this->next_page = $next_page;
    }

    public function getPeople(): array
    {
        return $this->people;
    }

    public function setPeople(array $people): void
    {
        $this->people = $people;
    }
}
