<?php

namespace App\Clients\Swapi;

use Exception;
use App\Entities\Swapi\Planet;
use App\Entities\Swapi\Person;
use App\Entities\Swapi\Species;
use Illuminate\Support\Facades\Http;
use App\Clients\Swapi\Responses\GetPeopleResponse;
use App\Clients\Swapi\Responses\GetPlanetsResponse;
use App\Clients\Swapi\Responses\GetSpeciesResponse;

class SwapiClient
{
    private const BASE_URL = 'https://swapi.py4e.com/api';

    public function getPlanets(int $page = 1): GetPlanetsResponse
    {
        $response = Http::baseUrl(self::BASE_URL)->get('/planets', ['page' => $page]);

        if ($response->failed()) {
            throw new Exception('There was an error fetching the planets');
        }

        $planets = [];

        foreach ($response->json('results') as $result) {
            $planet = new Planet();

            $planet->fill($result);

            $planets[] = $planet;
        }

        $getPlanetsResponse = new GetPlanetsResponse();

        $getPlanetsResponse->setPage($page);
        $getPlanetsResponse->setPreviousPage($response->json('previous') ? $page - 1 : null);
        $getPlanetsResponse->setNextPage($response->json('next') ? $page + 1 : null);

        $getPlanetsResponse->setCount($response->json('count'));
        $getPlanetsResponse->setPlanets($planets);

        return $getPlanetsResponse;
    }

    public function getPeople(int $page = 1): GetPeopleResponse
    {
        $response = Http::baseUrl(self::BASE_URL)->get('/people', ['page' => $page]);

        if ($response->failed()) {
            throw new Exception('There was an error fetching the people');
        }

        $people = [];

        foreach ($response->json('results') as $result) {
            $person = new Person();

            $person->fill($result);

            $people[] = $person;
        }

        $getPeopleResponse = new GetPeopleResponse();

        $getPeopleResponse->setPage($page);
        $getPeopleResponse->setPreviousPage($response->json('previous') ? $page - 1 : null);
        $getPeopleResponse->setNextPage($response->json('next') ? $page + 1 : null);

        $getPeopleResponse->setCount($response->json('count'));
        $getPeopleResponse->setPeople($people);

        return $getPeopleResponse;
    }

    public function getSpecies(int $page = 1): GetSpeciesResponse
    {
        $response = Http::baseUrl(self::BASE_URL)->get('/species', ['page' => $page]);

        if ($response->failed()) {
            throw new Exception('There was an error fetching the species');
        }

        $species = [];

        foreach ($response->json('results') as $result) {
            $speciesEntity = new Species();

            $speciesEntity->fill($result);

            $species[] = $speciesEntity;
        }

        $getSpeciesResponse = new GetSpeciesResponse();

        $getSpeciesResponse->setPage($page);
        $getSpeciesResponse->setPreviousPage($response->json('previous') ? $page - 1 : null);
        $getSpeciesResponse->setNextPage($response->json('next') ? $page + 1 : null);

        $getSpeciesResponse->setCount($response->json('count'));
        $getSpeciesResponse->setSpecies($species);

        return $getSpeciesResponse;
    }
}
