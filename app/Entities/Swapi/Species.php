<?php

namespace App\Entities\Swapi;

use App\Entities\Entity;

class Species extends Entity
{
    protected int $id;

    protected string $url;

    protected string $name;

    protected string $classification;

    protected string $designation;

    protected string $average_height;

    protected string $average_lifespan;

    protected string $eye_colors;

    protected string $hair_colors;

    protected string $skin_colors;

    protected string $language;

    protected ?int $homeworld;

    protected array $people;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): void
    {
        $this->url = $url;

        preg_match('/\/species\/(\d+)/', $url, $matches);

        $this->setId($matches[1]);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getClassification(): string
    {
        return $this->classification;
    }

    public function setClassification(string $classification): void
    {
        $this->classification = $classification;
    }

    public function getDesignation(): string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): void
    {
        $this->designation = $designation;
    }

    public function getAverageHeight(): string
    {
        return $this->average_height;
    }

    public function setAverageHeight(string $average_height): void
    {
        $this->average_height = $average_height;
    }

    public function getAverageLifespan(): string
    {
        return $this->average_lifespan;
    }

    public function setAverageLifespan(string $average_lifespan): void
    {
        $this->average_lifespan = $average_lifespan;
    }

    public function getEyeColors(): string
    {
        return $this->eye_colors;
    }

    public function setEyeColors(string $eye_colors): void
    {
        $this->eye_colors = $eye_colors;
    }

    public function getHairColors(): string
    {
        return $this->hair_colors;
    }

    public function setHairColors(string $hair_colors): void
    {
        $this->hair_colors = $hair_colors;
    }

    public function getSkinColors(): string
    {
        return $this->skin_colors;
    }

    public function setSkinColors(string $skin_colors): void
    {
        $this->skin_colors = $skin_colors;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function setLanguage(string $language): void
    {
        $this->language = $language;
    }

    public function getHomeworld(): ?int
    {
        return $this->homeworld;
    }

    public function setHomeworld(string|int|null $homeworld): void
    {
        if (!$homeworld) {
            $this->homeworld = $homeworld;

            return;
        }

        if (is_int($homeworld)) {
            $this->homeworld = $homeworld;

            return;
        }

        preg_match('/\/planets\/(\d+)/', $homeworld, $matches);

        $this->homeworld = $matches[1];
    }

    public function getPeople(): array
    {
        return $this->people;
    }

    public function setPeople(array $people): void
    {
        $matches = array_reduce($people, static function ($m, $resident) {
            if (preg_match('/\/people\/(\d+)/', $resident, $matches)) {
                $m[] = (int)$matches[1];
            }

            return $m;
        });

        if (!$matches) {
            $matches = [];
        }

        $this->people = $matches;
    }
}
