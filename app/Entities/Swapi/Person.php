<?php

namespace App\Entities\Swapi;

use App\Entities\Entity;

class Person extends Entity
{
    protected int $id;

    protected string $url;

    protected string $name;

    protected string $birth_year;

    protected string $eye_color;

    protected string $gender;

    protected string $hair_color;

    protected string $height;

    protected string $mass;

    protected string $skin_color;

    protected int $homeworld;

    protected array $species;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): void
    {
        $this->url = $url;

        preg_match('/\/people\/(\d+)/', $url, $matches);

        $this->setId($matches[1]);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getBirthYear(): string
    {
        return $this->birth_year;
    }

    public function setBirthYear(string $birth_year): void
    {
        $this->birth_year = $birth_year;
    }

    public function getEyeColor(): string
    {
        return $this->eye_color;
    }

    public function setEyeColor(string $eye_color): void
    {
        $this->eye_color = $eye_color;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function setGender(string $gender): void
    {
        $this->gender = $gender;
    }

    public function getHairColor(): string
    {
        return $this->hair_color;
    }

    public function setHairColor(string $hair_color): void
    {
        $this->hair_color = $hair_color;
    }

    public function getHeight(): string
    {
        return $this->height;
    }

    public function setHeight(string $height): void
    {
        $this->height = $height;
    }

    public function getMass(): string
    {
        return $this->mass;
    }

    public function setMass(string $mass): void
    {
        $this->mass = $mass;
    }

    public function getSkinColor(): string
    {
        return $this->skin_color;
    }

    public function setSkinColor(string $skin_color): void
    {
        $this->skin_color = $skin_color;
    }

    public function getHomeworld(): int
    {
        return $this->homeworld;
    }

    public function setHomeworld(string|int $homeworld): void
    {
        if (is_int($homeworld)) {
            $this->homeworld = $homeworld;

            return;
        }

        preg_match('/\/planets\/(\d+)/', $homeworld, $matches);

        $this->homeworld = $matches[1];
    }

    public function getSpecies(): array
    {
        return $this->species;
    }

    public function setSpecies(array $species): void
    {
        $matches = array_reduce($species, static function ($m, $resident) {
            if (preg_match('/\/species\/(\d+)/', $resident, $matches)) {
                $m[] = (int)$matches[1];
            }

            return $m;
        });

        if (!$matches) {
            $matches = [];
        }

        $this->species = $matches;
    }
}
