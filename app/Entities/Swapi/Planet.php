<?php

namespace App\Entities\Swapi;

use App\Entities\Entity;

class Planet extends Entity
{
    protected int $id;

    protected string $url;

    protected string $name;

    protected string $diameter;

    protected string $rotation_period;

    protected string $orbital_period;

    protected string $gravity;

    protected string $population;

    protected string $climate;

    protected string $terrain;

    protected string $surface_water;

    protected array $residents;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): void
    {
        $this->url = $url;

        preg_match('/\/planets\/(\d+)/', $url, $matches);

        $this->setId($matches[1]);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDiameter(): string
    {
        return $this->diameter;
    }

    public function setDiameter(string $diameter): void
    {
        $this->diameter = $diameter;
    }

    public function getRotationPeriod(): string
    {
        return $this->rotation_period;
    }

    public function setRotationPeriod(string $rotation_period): void
    {
        $this->rotation_period = $rotation_period;
    }

    public function getOrbitalPeriod(): string
    {
        return $this->orbital_period;
    }

    public function setOrbitalPeriod(string $orbital_period): void
    {
        $this->orbital_period = $orbital_period;
    }

    public function getGravity(): string
    {
        return $this->gravity;
    }

    public function setGravity(string $gravity): void
    {
        $this->gravity = $gravity;
    }

    public function getPopulation(): string
    {
        return $this->population;
    }

    public function setPopulation(string $population): void
    {
        $this->population = $population;
    }

    public function getClimate(): string
    {
        return $this->climate;
    }

    public function setClimate(string $climate): void
    {
        $this->climate = $climate;
    }

    public function getTerrain(): string
    {
        return $this->terrain;
    }

    public function setTerrain(string $terrain): void
    {
        $this->terrain = $terrain;
    }

    public function getSurfaceWater(): string
    {
        return $this->surface_water;
    }

    public function setSurfaceWater(string $surface_water): void
    {
        $this->surface_water = $surface_water;
    }

    public function getResidents(): array
    {
        return $this->residents;
    }

    public function setResidents(array $residents): void
    {
        $matches = array_reduce($residents, static function ($m, $resident) {
            if (preg_match('/\/people\/(\d+)/', $resident, $matches)) {
                $m[] = (int)$matches[1];
            }

            return $m;
        });

        if (!$matches) {
            $matches = [];
        }

        $this->residents = $matches;
    }
}
