<?php

namespace App\Providers;

use App\Services\Log\LogService;
use App\Services\EloquentService;
use App\Services\Planet\PlanetService;
use App\Services\Person\PersonService;
use Illuminate\Support\ServiceProvider;
use App\Repositories\EloquentRepository;
use App\Services\Species\SpeciesService;
use App\Services\Log\LogServiceInterface;
use App\Services\EloquentServiceInterface;
use App\Repositories\Log\LogCacheRepository;
use App\Services\Planet\PlanetServiceInterface;
use App\Services\Person\PersonServiceInterface;
use App\Repositories\Log\LogRepositoryInterface;
use App\Repositories\EloquentRepositoryInterface;
use App\Services\Species\SpeciesServiceInterface;
use App\Repositories\Planet\PlanetCacheRepository;
use App\Repositories\Person\PersonCacheRepository;
use App\Repositories\Species\SpeciesCacheRepository;
use App\Repositories\Planet\PlanetRepositoryInterface;
use App\Repositories\Person\PersonRepositoryInterface;
use App\Repositories\Species\SpeciesRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(EloquentRepositoryInterface::class, EloquentRepository::class);
        $this->app->bind(EloquentServiceInterface::class, EloquentService::class);

        $this->app->bind(LogRepositoryInterface::class, LogCacheRepository::class);
        $this->app->bind(LogServiceInterface::class, LogService::class);

        $this->app->bind(PersonRepositoryInterface::class, PersonCacheRepository::class);
        $this->app->bind(PersonServiceInterface::class, PersonService::class);

        $this->app->bind(PlanetRepositoryInterface::class, PlanetCacheRepository::class);
        $this->app->bind(PlanetServiceInterface::class, PlanetService::class);

        $this->app->bind(SpeciesRepositoryInterface::class, SpeciesCacheRepository::class);
        $this->app->bind(SpeciesServiceInterface::class, SpeciesService::class);
    }

    public function boot(): void
    {
        //
    }
}
