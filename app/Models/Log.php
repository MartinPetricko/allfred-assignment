<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use ESolution\DBEncryption\Traits\EncryptedAttribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Log extends Model
{
    use HasFactory;
    use EncryptedAttribute;

    protected $fillable = [
        'weather',
        'temperature',
        'mood',
        'location',
        'accomplishments',
        'observations',
        'health_condition',
        'ship_condition',
        'radio_communication',
        'inventory',
        'note',
    ];

    protected array $encryptable = [
        'weather',
        'temperature',
        'mood',
        'location',
        'accomplishments',
        'observations',
        'health_condition',
        'ship_condition',
        'radio_communication',
        'inventory',
        'note',
    ];
}
