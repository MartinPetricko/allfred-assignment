<?php

namespace App\Models;

use App\Filters\BaseFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Person extends Model
{
    use HasFactory;
    use Filterable;

    protected $fillable = [
        'name',
        'birth_year',
        'eye_color',
        'gender',
        'hair_color',
        'height',
        'mass',
        'skin_color',
    ];

    public function modelFilter(): ?string
    {
        return $this->provideFilter(BaseFilter::class);
    }

    //--Relationships---------------------------------------------------------------------------------------------------

    public function planet(): BelongsTo
    {
        return $this->belongsTo(Planet::class);
    }

    public function species(): BelongsToMany
    {
        return $this->belongsToMany(Species::class);
    }
}
