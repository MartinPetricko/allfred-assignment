<?php

namespace App\Models;

use App\Filters\BaseFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Species extends Model
{
    use HasFactory;
    use Filterable;

    protected $fillable = [
        'name',
        'language',
        'average_height',
        'average_lifespan',
        'classification',
        'designation',
        'eye_colors',
        'hair_colors',
        'skin_colors',
    ];

    public function modelFilter(): ?string
    {
        return $this->provideFilter(BaseFilter::class);
    }

    //--Relationships---------------------------------------------------------------------------------------------------

    public function planet(): BelongsTo
    {
        return $this->belongsTo(Planet::class);
    }

    public function people(): BelongsToMany
    {
        return $this->belongsToMany(Person::class);
    }
}
