<?php

namespace App\Models;

use App\Filters\PlanetFilter;
use EloquentFilter\Filterable;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Repositories\Species\SpeciesRepositoryInterface;

class Planet extends Model
{
    use HasFactory;
    use Filterable;

    protected $fillable = [
        'name',
        'diameter',
        'rotation_period',
        'orbital_period',
        'gravity',
        'population',
        'climate',
        'terrain',
        'surface_water',
    ];

    public function modelFilter(): ?string
    {
        return $this->provideFilter(PlanetFilter::class);
    }

    //--Relationships---------------------------------------------------------------------------------------------------

    public function people(): HasMany
    {
        return $this->hasMany(Person::class);
    }

    //--Attributes------------------------------------------------------------------------------------------------------

    protected function species(): Attribute
    {
        return new Attribute(
            get: function () {
                $species_ids = DB::table('person_species')->whereIn('person_id', $this->people->pluck('id'))->pluck('species_id');

                return app(SpeciesRepositoryInterface::class)->find($species_ids);
            },
        );
    }
}
