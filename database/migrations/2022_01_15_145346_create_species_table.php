<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpeciesTable extends Migration
{
    public function up(): void
    {
        Schema::create('species', function (Blueprint $table) {
            $table->id();

            $table->foreignId('planet_id')->nullable()->constrained();

            $table->string('name');
            $table->string('language');
            $table->string('average_height');
            $table->string('average_lifespan');
            $table->string('classification');
            $table->string('designation');
            $table->string('eye_colors');
            $table->string('hair_colors');
            $table->string('skin_colors');

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('species');
    }
}
