<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanetsTable extends Migration
{
    public function up(): void
    {
        Schema::create('planets', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->integer('diameter')->nullable()->index();
            $table->integer('rotation_period')->nullable()->index();
            $table->integer('orbital_period')->nullable();
            $table->string('gravity')->index();
            $table->bigInteger('population')->nullable();
            $table->string('climate');
            $table->string('terrain');
            $table->decimal('surface_water')->nullable();

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('planets');
    }
}
