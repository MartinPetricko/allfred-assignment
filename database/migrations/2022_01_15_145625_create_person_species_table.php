<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonSpeciesTable extends Migration
{
    public function up(): void
    {
        Schema::create('person_species', function (Blueprint $table) {
            $table->foreignId('person_id')->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignId('species_id')->constrained()->cascadeOnUpdate()->cascadeOnDelete();

            $table->unique(['person_id', 'species_id']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('person_species');
    }
}
