<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    public function up(): void
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->id();

            $table->string('weather');
            $table->string('temperature');
            $table->string('mood');
            $table->string('location')->nullable();
            $table->string('accomplishments', 1024)->nullable();
            $table->string('observations', 2048)->nullable();
            $table->text('health_condition');
            $table->text('ship_condition')->nullable();
            $table->text('radio_communication')->nullable();
            $table->text('inventory');
            $table->text('note');

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('logs');
    }
}
