## API documentation

### Logbook

#### Fetch all records

```
GET /api/logs
```

Response:

```JSON
{
    "data": [
        {
            "id": 1,
            "weather": "Sunny",
            "temperature": "26 °C",
            "mood": "Happy",
            "location": "Unknown",
            "accomplishments": "Built a shelter.",
            "observations": "Sand everywhere.",
            "health_condition": "Healthy",
            "ship_condition": "Damaged",
            "radio_communication": "None",
            "inventory": "1l water, food, batteries",
            "note": "First day. Bad day.",
            "created_at": "2022-01-18T17:40:16.000000Z",
            "updated_at": "2022-01-18T17:40:16.000000Z"
        }
    ]
}
```

#### Store record

```
POST /api/logs
```

Body:

```JSON
{
    "weather": "Sunny",
    "temperature": "26 °C",
    "mood": "Happy",
    "location": "Unknown",
    "accomplishments": "Built a shelter.",
    "observations": "Sand everywhere.",
    "health_condition": "Healthy",
    "ship_condition": "Damaged",
    "radio_communication": "None",
    "inventory": "1l water, food, batteries",
    "note": "First day. Bad day."
}
```

Response:

```JSON
{
    "data": {
        "id": 1,
        "weather": "Sunny",
        "temperature": "26 °C",
        "mood": "Happy",
        "location": "Unknown",
        "accomplishments": "Built a shelter.",
        "observations": "Sand everywhere.",
        "health_condition": "Healthy",
        "ship_condition": "Damaged",
        "radio_communication": "None",
        "inventory": "1l water, food, batteries",
        "note": "First day. Bad day.",
        "created_at": "2022-01-18T17:40:16.000000Z",
        "updated_at": "2022-01-18T17:40:16.000000Z"
    }
}
```
